const colorRandomizer = () => {
	return Math.floor(Math.random()*16777215).toString(16);
}

export { colorRandomizer }


// Function for converting string data from the endpoint to numbers
const toNum = (str) => {

	const stringArray = [...str];
	const filteredArr = stringArray.filter(element => element !== ",")

	return parseInt(filteredArr.reduce((x, y) => x + y))
}

export {toNum}