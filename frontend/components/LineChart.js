import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import { Card } from 'react-bootstrap';
import moment from 'moment';
import { colorRandomizer } from '../helpers';

export default function LineExample({dataLineChart}) {

	const [lineChartLabels, setLineChartLabels] = useState([]);
	const [lineChartData, setLineChartData] = useState([]);

	useEffect(() => {

		setLineChartLabels(dataLineChart.map(element => moment(element.date).format("MMM DD 'YY")  ))

		setLineChartData(dataLineChart.map(element => element.amount))

	}, [dataLineChart])

	const data = {
		labels: lineChartLabels,
		datasets: [
			{
				label: 'Balance',
				fill: false,
				lineTension: 0.1,
				backgroundColor: 'rgba(0, 0, 0, .5)',
				borderColor: `#${colorRandomizer()}`,
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: 'rgba(75,192,192,1)',
				pointBackgroundColor: '#fff',
				pointBorderWidth: 4,
				pointHoverRadius: 6,
				pointHoverBackgroundColor: 'rgba(75,192,192,1)',
				pointHoverBorderColor: 'rgba(220,220,220,1)',
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: lineChartData
			}
		]
	};

    return (
      <div>
      <Card className='py-5'>
      	<h3 className='text-center'>Budget Trend</h3>
        <Line data={data} />
        </Card>
      </div>
    );
};