import React, { useState, useEffect } from 'react';
import { Button, Table, Row, Col, Card } from 'react-bootstrap';
import moment from 'moment';
import Router from 'next/router';


export default function ShowAddRecord(){

	const addTransaction = () => {
        Router.push('/records/AddRecord')
    }

    const addNewCategory = () => {
    	Router.push('/categories')
    }

	const [records, setRecords] = useState([]);

	// Get the user's records when the component mounts
	useEffect(() => {

		const options = {
		headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
		};
		
		fetch(`${process.env.NEXT_PUBLIC_API_URI}/api/users/details`, options)
		.then(res => res.json())
        .then(data => {
        	
        	if (data._id){ // JWT validated
        		setRecords(data.records)
        	} else { // JWT is invalid or non-existent
        		setRecords([])
        	}
        })
	}, [])
	

	return(
		<React.Fragment>
			<Card className="w-100 d-flex text-center mt-3">
				<Card.Header className="text-center" ><strong>Transactions List</strong></Card.Header>
				<Row className="block justify-content-center">
					<Col>
						<Button className="mt-3 bg-warning" onClick={addTransaction}>
							Post New Transaction
						</Button>
					</Col>
					<Col>
						<Button className="mt-3 bg-warning" onClick={addNewCategory}>
							Add New Category
						</Button>
					</Col>
				</Row>
				<Card.Body>
					<Table striped bordered hover responsive>
						<thead>
							<tr>
								<th>Date</th>
								<th>Name</th>
								<th>Description</th>
								<th>Type</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							{records.reverse().map(record => {
								return(	
									<tr key={record._id}>
										<td>{moment(record.date).format('MMMM DD, YYYY')}</td>
										<td>{record.categoryName}</td>
										<td>{record.description}</td>
										<td>{record.categoryType}</td>
										<td>{record.amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</td>
									</tr>
								)
							})}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		</React.Fragment>
	)
}