import React, { useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Link from 'next/link';
import UserContext from '../UserContext';

export default function NavBar(){

	const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			{(user.id === null)
				?
				<Link href="/">
					<a className="navbar-brand">Budget Tracker</a>
				</Link>
				:
				<Link href="/home">
					<a className="navbar-brand">Budget Tracker</a>
				</Link>
			}			
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="mr-auto">
					{(user.id !== null)
						? 
						<React.Fragment>
							<Link href="/categories">
								<a className="nav-link" role="button">Categories</a>
							</Link>
							<Link href="/records">
								<a className="nav-link" role="button">Records</a>
							</Link>
							<Link href="/analytics">
								<a className="nav-link" role="button">Analytics</a>
							</Link>
							<Link href="/breakdown">
								<a className="nav-link" role="button">Breakdown</a>
							</Link>
							<Link href="/trend">
								<a className="nav-link" role="button">Trend</a>
							</Link>
							<Link href="/logout">
								<a className="nav-link" role="button">Logout</a>
							</Link>
						</React.Fragment>
						:
						<React.Fragment>
							<Link href="/">
								<a className="nav-link" role="button">Login</a>
							</Link>
							<Link href="/register">
								<a className="nav-link" role="button">Register</a>
							</Link>
						</React.Fragment>
					}			
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}