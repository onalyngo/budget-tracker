const express = require ('express')
const router = express.Router()
const auth = require('../auth')

const UserController = require('../controllers/user')

// Check if email exist
router.post('/email-exists', (req, res) => {
	UserController.emailExists(req.body).then(result => res.send (result))
})

// User Registration
router.post('/', (req, res) => {
	UserController.register(req.body).then(result => res.send(result))
})

// Login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result))
})

//for getting private user profile info
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.getPrivate({ userId: user.id }).then(user => res.send(user))
})

// Verify Google Token
router.post('/verify-google-id-token', async (req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

// Adding Categories
router.post('/categories', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        category: {
            name: req.body.name,
            type: req.body.type
        }
    }
    UserController.addCategory(params).then(result => res.send(result))
})

//for recording user transactions
router.post('/records', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        record: {
            categoryName: req.body.categoryName,
            categoryType: req.body.categoryType,
            description: req.body.description,
            amount: req.body.amount
        }
    }
    UserController.addRecord(params).then(result => res.send(result))
})

// to edit record
// router.put('/update', auth.verify, (req, res) => {
//     UserController.update(req.body).then(result => res.send(result))
// })

// // to delete record
// router.delete('/records/:recordsId', auth.verify, (req, res) => {
//     const recordsId = req.params.recordsId
//     UserController.archive({ recordsId }).then(result => res.send(result))
// })


module.exports = router