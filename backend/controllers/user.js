const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const { OAuth2Client } = require('google-auth-library')
const clientId = '125345612245-83svh3mnhv0llg0ac25jmqr086ff369o.apps.googleusercontent.com'
// const sms = require('../sms')
// const nodemailer = require('nodemailer')


// Function to catch & handle errors
const errCatcher = err => console.log(err)

// Function for finding duplicate emails
module.exports.emailExists = (params) => {
	//find a user document with matching email
	return User.find({ email: params.email })
	.then(result => {
		//if match found, return true
		return result.length > 0 ? true : false
	})
	.catch(errCatcher)
}

// Function to User Registration
module.exports.register = (params) => {
	//instantiate a new user object
	let newUser = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'email'
	})

 	// save user object as a new document
	return newUser.save()
	.then((user, err) => {
		// if err generated, return false otherwise return true
		return (err) ? false : true
	})
}

// Login
module.exports.login = (params) => {
	//find a user with matching email
	return User.findOne({ email: params.email })
	.then(user => {
		//if no match found, return false
		if (user === null) return false

		//check if submitted password matches password on record
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		//if matching password
		if(isPasswordMatched){
			//generate JWT
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}
	})
	.catch(errCatcher)
}

//function for getting details of a user based on a decoded token
module.exports.getPrivate = (params) => {
	return User.findById(params.userId)
	.then(user => {
		//clear the password property of the user object for security
		user.password = undefined
		return user
	})
	.catch(errCatcher)
}

// Google Login
module.exports.verifyGoogleTokenId = async (tokenId) => {
	// Client object from the 'google-auth-library' package used to authorize and authenticate communication with Google APIs
	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})

	console.log(data.payload);

	if (data.payload.email_verified === true) {
		// .exec() is a mongoose object method that is used for stack tracing in this case, can be removed and the code will still work.
		const user = await User.findOne({ email: data.payload.email }).exec();

		console.log(user);
		
		if (user !== null) {
			if (user.loginType === "google") {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			} else {
				return { error: "login-type-error" }
			}
		} else {
			const newUser = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: "google"
			})

			return newUser.save().then((user, err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})
		}
	} else {
		return { error: "google-auth-error" }
	}
}

// Function for Adding Category Name & Category Type
module.exports.addCategory = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.categories.push(params.category)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
        })
        .catch(errCatcher)
    })
    .catch(errCatcher)
}


// Function for Adding Record
module.exports.addRecord = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err) return false
        user.records.push(params.record)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
        })
        .catch(errCatcher)
    })
    .catch(errCatcher)
}

// to Edit record
module.exports.update = (params) => {
	const updates = {
		categoryName: params.categoryName,
		categoryType: params.categoryType,
		description: params.description,
		amount: params.amount
	}
	return User.findByIdAndUpdate(params.recordsId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

// // to Delete record
// module.exports.archive = (params) => {
// 	const updates = { isActive: false }

// 	return User.findByIdAndUpdate(params.recordsId, updates).then((doc, err) => {
// 		return (err) ? false : true
// 	})
// }