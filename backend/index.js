const express = require('express');
const app = express();
require('dotenv').config();
const mongoose = require('mongoose');
const cors = require('cors');

const port = process.env.PORT;
const mongodbCloud = process.env.DB_MONGODB;

const userRoutes = require('./routes/user');

//add whitelisted origins here
// const corsOptions = {
// 	origin: ['http://localhost:3000'],
// 	optionsSuccessStatus: 200//for compatibility with older browsers
// }

app.use(cors());
app.options("*", cors());

// next.config.js
module.exports = {
  async headers() {
    return [
      {
        // matching all API routes
        source: "/api/:path*",
        headers: [
          { key: "Access-Control-Allow-Credentials", value: "true" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Access-Control-Allow-Methods", value: "GET,OPTIONS,PATCH,DELETE,POST,PUT" },
          { key: "Access-Control-Allow-Headers", value: "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version" },
        ]
      }
    ]
  }
};


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect(mongodbCloud, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
})

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//use cors as a middleware passing in options
app.use('/api/users', userRoutes);

app.listen(port, () => {
	console.log(`API is now online on port ${ port }`);
})