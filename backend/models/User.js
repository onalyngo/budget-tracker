// User Model
const mongoose = require('mongoose')
const moment = require('moment')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		// required: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		// required: [true, 'Last name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.'],
		unique: true
	},
	password: {
		type: String,
		// required: [true, 'Password is required.']
	},
	mobileNo: {
		type: String,
		// required: [true, 'Mobile number is required.']
	},
	loginType: {
		type: String,
		required: [true, 'Login type is required.']
	},
	categories: [
		{
			name: {
				type: String,
				required: [true, 'Category name is required.'],
				},
			type: {
				type: String,
				required: [true, 'Category type is required.']
			}
		}
	],
	records: [
		{
			categoryName: {
				type: String,
				required: [true, 'Category name is required.'],
				},
			categoryType: {
				type: String,
				required: [true, 'Category type is required.'],
				default: 'Income'
			},
			description: {
				type: String,
				required: [true, 'Description is required.']
			},
			amount: {
				type: Number,
                required: [true, 'Amount is required']
			},
			date: {
				type: Date,
				default: moment(Date.now()).format('MMMM DD, YYYY')
			}
		}
	]
})

module.exports = mongoose.model('user', userSchema)